<?php

namespace Infotechnohelp\FileWizard;

/**
 * Class FileWizard
 * @package Infotechnohelp\FileWizard
 */
class FileWizard
{
    /**
     * @param string $phpDumpPath
     * @return PhpDump
     */
    public function createPhpDump(string $phpDumpPath): PhpDump
    {
        return new PhpDump("$phpDumpPath.php");
    }
}