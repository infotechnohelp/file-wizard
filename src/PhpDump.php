<?php

namespace Infotechnohelp\FileWizard;

/**
 * Class PhpDump
 * @package Infotechnohelp\FileWizard
 */
class PhpDump
{
    /**
     * @var string
     */
    private $path;

    /**
     * PhpDump constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return array
     */
    public function read(): array
    {
        return require $this->path;
    }

    /**
     * @param array $array
     * @return PhpDump
     */
    public function write(array $array): self
    {
        $file = fopen($this->path, 'w');
        fwrite($file, "<?php\nreturn " . var_export($array, true) . ";");
        fclose($file);

        return $this;
    }

    /**
     * @param $item
     * @param string|null $key
     * @return PhpDump
     */
    public function push($item, string $key = null): self
    {
        $current = $this->read();

        if ($key === null) {
            $current[] = $item;
            $this->write($current);
            return $this;
        }

        $current[$key][] = $item;
        $this->write($current);

        return $this;
    }
}